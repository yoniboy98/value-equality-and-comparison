﻿using System;
using System.Collections;

namespace ValueEqualityAndComparison___Yoni_Vindelinckx
{
    class Program
    {
        static void Main(string[] args)
        {
            //  valueEqualityWithInt();
            //    valueEqualityWithBool();
            //  valueEqualityWithFloat();
          //   valueEqualityWithStrings();

            //    Console.WriteLine("met operator:" + AreWeEquals(4,4));

            //  Console.WriteLine("met operator:" + AreWeEqualsMethode(4, 4));
            //   AreStringsEqual();
            //  StructuralEqualityArray();
         //   StructuralComparisonArray();
            //   overrideEquals();
            //   interfaceIcomparable();
              overrideEquals();
            //  arrayComparison();
        }

        private static void valueEqualityWithInt()
        {
            int a = 1;
            int b = 1;

            if (a == b)
            {
                Console.WriteLine($"{a} and {b} zijn gelijk");
            } else
            {
                Console.WriteLine($"{a} and {b} zijn niet gelijk");
            }

        }
        private static void valueEqualityWithBool()
        {
            bool a = true;
            bool b = true;

            if (a == b)
            {
                Console.WriteLine($"{a} and {b} zijn gelijk");
            }
            else
            {
                Console.WriteLine($"{a} and {b} zijn niet gelijk");
            }

        }

        private static void valueEqualityWithFloat()
        {
            float a = 6.0000000f;
            float b = 6.0000001f;

            if (a == b)
            {
                Console.WriteLine($"{a} and {b} zijn gelijk");
            }
            else
            {
                Console.WriteLine($"{a} and {b} zijn niet gelijk");
            }

        }

        private static void valueEqualityWithStrings()
        {
            string a = "peer";
            string b = "peer";


            if (a.Equals(b))
            {
                Console.WriteLine($"{a} and {b} zijn gelijk");

            }
            else
            {
                Console.WriteLine($"{a} and {b} zijn niet gelijk");
            }

        }

        static bool AreWeEquals(int x, int y)
        {
            return x == y;
        }
        static bool AreWeEqualsMethode(int x, int y)
        {
            return x.Equals(y);
        }


        private static void AreStringsEqual()
        {
            string appel = "appel";
            string appel2 = string.Copy(appel);

            Console.WriteLine(appel.Equals((object)appel2));
        }

        private static void StructuralEqualityArray()
        {
            string[] lol = { "dit", "is", "een", "taak" };
            string[] lol2 = { "dit", "is", "een", "taak" };

            //     Console.WriteLine(lol == lol2);
            //   Console.WriteLine(lol.Equals(lol2));

            var array = (IStructuralEquatable)lol;
            bool structEqual = array.Equals(lol2, StringComparer.Ordinal);
            Console.WriteLine(structEqual);

        }


        private static void StructuralComparisonArray()
        {
            string[] lol = { "dit", "is", "den", "taak" };
            string[] lol2 = { "dit", "is", "een", "taak" };

            //     Console.WriteLine(lol == lol2);
            //   Console.WriteLine(lol.Equals(lol2));

            var array = (IStructuralComparable)lol;
            int structEqual = array.CompareTo(lol2, StringComparer.OrdinalIgnoreCase);
            Console.WriteLine(structEqual);

        }



        private static void overrideEquals() {

            spelers speler = new spelers("Yoni");
            spelers speler2 = new spelers("Yoni");


            Console.WriteLine(speler.Equals(speler2));




        }


        private static void interfaceIEquatable()
        {
            int een = 1;
            int twee = 2;
            int eenopnieuw = 1;

            Console.WriteLine(een.Equals(eenopnieuw));
            Console.WriteLine(een.Equals(twee));

        }

        private static void interfaceIcomparable()
        {
            int appel = 1;
            int peer = 2;

            Console.WriteLine(appel.CompareTo(peer));
            Console.WriteLine(peer.CompareTo(appel));
            Console.WriteLine(appel.CompareTo(appel));
        }

        /*   private static void arrayComparison() {

                string[] spullen =
                {
                    "yoni",
                    "banaan",
                    "appel",
                    "tafel"
                };

                Array.Sort(spullen);

                foreach( var item in spullen)
                    Console.WriteLine(item);

            }*/

        private static void arrayComparison()
        {

            spelers[] spullen =
            {
               new spelers( "yoni"),
               new spelers( "banaan"),
               new spelers( "appel"),
                new spelers( "tafel"),
            };

            Array.Sort(spullen);

            foreach (var item in spullen)
                Console.WriteLine(item);

        }

        private static void Comparen()
        {
            int a = 1;
            int b = 2;


            object lol = new object();
            object lol2 = new object();

            Console.WriteLine(a < b);
      
            Console.WriteLine(lol < lol2);
        }

    }
    
}
