﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ValueEqualityAndComparison___Yoni_Vindelinckx
{
    struct spelers
    {
        public string naam;

        public spelers(string naam)
        {
            this.naam = naam;
        }

        public override bool Equals(object obj)
        {
            return this.naam == (string)obj.ToString();
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return naam;
        }

    }
}
